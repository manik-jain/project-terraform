resource "google_sql_database" "database" {
    name = "assignment_1"
    instance = google_sql_database_instance.instance.name
}

resource "google_sql_user" "users" {
    name = var.db_user
    instance = google_sql_database_instance.instance.name
    host = "%"
    password = var.db_pass
}

resource "google_sql_database_instance" "instance" {
    name = "assignment-2"
    region  = "us-central1"
    settings {
        tier = "db-f1-micro"
    }
}