# Project-Terraform

This project takes care of creating a default Infrastructure for backend support with Google Cloud Platform. 
The project uses Terraform as the underlying technology for creating the Infrastructure.
Any changes pushed/merged to master branch will be deployed by default to Google cloud using the Continuous Deployment.

#Commands used : 

1. terraform init : to initialise the Terraform in the project directory
2. terraform plan : prepare the changes that needs to be performed
3. terraform apply : perform the changes to the remote GCP cloud

