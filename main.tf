terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
}

provider "google" {

  credentials = file("credentials/dev.json")

  project = "assignment-1-302006"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_cloud_run_service" "default" {
  name     = "cloudrun-srv"
  location = "us-central1"

  template {
    spec {
      containers {
        image = "gcr.io/assignment-1-302006/project-backend"
        env {
            name = "ENV_PORT"
            value = "8080"
        }

        env {
            name = "DATABASE_USER"
            value = google_sql_user.users.name
        }

        env {
            name = "DATABASE_PASSWORD"
            value = google_sql_user.users.password
        }

        env {
            name = "DATABASE_SOCKET"
            value = "/cloudsql/assignment-1-302006:us-central1:assignment-2"
        }

        env {
            name = "DATABASE_NAME"
            value = google_sql_database.database.name
        }
      }
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/maxScale"      = "10"
        "run.googleapis.com/cloudsql-instances" = google_sql_database_instance.instance.connection_name
        "run.googleapis.com/client-name"        = "terraform"
      }
    }
  }
  
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location    = google_cloud_run_service.default.location
  project     = google_cloud_run_service.default.project
  service     = google_cloud_run_service.default.name

  policy_data = data.google_iam_policy.noauth.policy_data
}